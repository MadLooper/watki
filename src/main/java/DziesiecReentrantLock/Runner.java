package DziesiecReentrantLock;
import java.util.Scanner;
import java.util.concurrent.locks.*;
public class Runner {

    private int count =0;
    private Lock lock= new ReentrantLock();
private Condition condition= lock.newCondition();
    private void increment(){
        for (int i=0;i<10000;i++){
            count++;
        }
    }
    public void firstThread() throws InterruptedException {
        lock.lock();
        condition.await();

        try {
            increment();
        } finally {
            lock.unlock();
        }
    }
    public void secThread() throws InterruptedException{
        Thread.sleep(1000);
        lock.lock();

        System.out.println("Press the return key!");
        new Scanner(System.in).nextLine();
        System.out.println("Got ur return key!");

        condition.signal();
        try {
            increment();
        } finally {
            lock.unlock();
        }
    }
    public void finished() throws InterruptedException{
        System.out.println("Count is: "+ count);
    }

}
