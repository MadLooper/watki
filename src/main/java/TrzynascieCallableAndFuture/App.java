package TrzynascieCallableAndFuture;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by KW on 11/19/2017.
 */
public class App {
    public static void main(String[] args) {
        ExecutorService executors = Executors.newCachedThreadPool();
        Future<Integer> future = executors.submit(new Callable<Integer>() {
            public Integer call() throws Exception {
                System.out.println("starting... ");
                Random random = new Random();
                int duration = random.nextInt(3000);
               if (duration>2000){
                   throw new IOException("Sleeping for too long.");

               }
                try {
                    Thread.sleep(duration);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Finished. ");
                return duration;
            }
        });

        executors.shutdown();

        try {
            System.out.println("Result is: "+future.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
