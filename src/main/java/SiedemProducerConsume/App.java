package SiedemProducerConsume;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class App {
    private static BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(10);
    //KOLEJKA ZADAN DO WYWOLANIA (Czekajacych), FIFO, threadSafe

    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                producer();
            }
        });
        Thread t2 = new Thread(new Runnable() {
            public void run() {
                consumer();
            }
        });
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void producer(){
        Random random = new Random();
        while (true){

            try {
                queue.put(random.nextInt(100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    private static void consumer() {
        Random random = new Random();
        while (true){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (random.nextInt(10)==0){
                Integer value = null;
                try {
                    value = queue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Taken value: "+ value +"; Queue size is: "+queue.size());
            }
        }
    }
}
