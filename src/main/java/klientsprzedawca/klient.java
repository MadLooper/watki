package klientsprzedawca;

import java.util.concurrent.CountDownLatch;

public class klient implements Runnable{
    private CountDownLatch cdl;

    public klient(CountDownLatch cdl) {
        this.cdl = cdl;
    }

    public void run() {
        try {
            cdl.await();
            System.out.println("Odebralem towar");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
