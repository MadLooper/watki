package klientsprzedawca;

import java.util.concurrent.CountDownLatch;

public class Main {
    public static void main(String[] args) {
        CountDownLatch countDownLatch = new CountDownLatch(10000000);
        new Thread(new klient(countDownLatch)).start();
        new Thread(new sprzedawca(countDownLatch)).start();
    }
}