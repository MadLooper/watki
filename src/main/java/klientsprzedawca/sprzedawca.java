package klientsprzedawca;

import java.util.concurrent.CountDownLatch;

public class sprzedawca implements Runnable{
    private CountDownLatch cdl;

    public sprzedawca(CountDownLatch cdl) {
        this.cdl = cdl;
    }

    public void run() {
        System.out.println("Sprzedawca szukam towaru.");
        while (cdl.getCount()>0){
            cdl.countDown();
        }
    }
}
