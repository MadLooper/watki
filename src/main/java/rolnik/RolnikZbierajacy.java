package rolnik;

public class  RolnikZbierajacy extends Rolnik implements Runnable {
    private Pole pole;

    public RolnikZbierajacy(String name) {
        super(name);
    }

    public RolnikZbierajacy(String name, Pole pole) {
        super(name);
        this.pole = pole;
    }

    public void zbierajZboze() {

    }

    public void run() {
        while (!Thread.interrupted()) {
            pole.akcjaKoszenie();
pole.czekajNaKoszenie();
        }
    }
}
