package rolnik;

public abstract class Rolnik {
    String name;

    public Rolnik(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
