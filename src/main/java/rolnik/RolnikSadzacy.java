package rolnik;

public class RolnikSadzacy extends Rolnik implements Runnable{
    private Pole pole;

    public RolnikSadzacy(String name) {
        super(name);
    }

    public RolnikSadzacy(String name, Pole pole) {
        super(name);
        this.pole = pole;
    }

    public void sadzBuraki(){

    }

    public void run() {
        pole.akcjaSadzenie();
        while (!Thread.interrupted()){
            pole.akcjaSadzenie();
            try {
                pole.czekajNaSadzenie();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
