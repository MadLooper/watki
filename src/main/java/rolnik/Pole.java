package rolnik;

public class Pole {

    private boolean moznaSadzic = false;

    public synchronized void czekajNaSadzenie() throws InterruptedException {
        while (!moznaSadzic){
            wait();
        }
    }

    public synchronized void akcjaSadzenie() {
        System.out.println("Sieje zboze");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        moznaSadzic = false;
        notifyAll();
        //budzimy wszystkie watki
    }

    public synchronized void czekajNaKoszenie() {
        while (moznaSadzic) {
            try {
                wait();
                System.out.println("czekam na koszenie");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void akcjaKoszenie() {
        System.out.println("Kosze zboze");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        moznaSadzic = true;
        notifyAll();
    }
}