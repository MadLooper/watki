package rolnik;

public class Main {

    public static void main(String[] args) {
        Pole pole = new Pole();
        Runnable rolnikSadzacy = new RolnikSadzacy("asia", pole);
        Runnable rolnikZbierajacy = new RolnikZbierajacy("AndrzEj", pole);
        Thread t1 = new Thread(rolnikSadzacy);
        Thread t2 = new Thread(rolnikZbierajacy);
        t1.start();
        t2.start();
    }
}
//notify all - budzimy wszytkie watki