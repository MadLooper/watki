package SimpleBank;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by KW on 11/19/2017.
 */
public class App {
    public static void main(String[] args) {
        Account account = new Account(50000.0);
        Transaction transaction = new Transaction();
        ExecutorService executor = Executors.newFixedThreadPool(15);
        Scanner sc = new Scanner(System.in);

        System.out.println("Hello! Balance of ur acccount is: " + account.getBalance());
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                transaction.withdraw(account,50.0);
            } else {
                transaction.deposit(account,40.0);
            }
        }
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] splits = line.split(" ");
            if (splits[0].equals("add")) {
                transaction.deposit(account,(Double.parseDouble(splits[1])));
                System.out.println("Your balance: " + account.getBalance());
            } else if (splits[0].equals("sub")) {
                transaction.withdraw(account,(Double.parseDouble(splits[1])));
                System.out.println("Your balance account now is: "+ account.getBalance());
            } else if (splits[0].equals("balance")) {
               transaction.balance(account);
                System.out.println("Your balance account is: " + transaction.balance(account));
            } else if (splits[0].equals("finish")){
                System.out.println("Goodbye! ");
            }
        }
    }

}
