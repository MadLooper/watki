package SimpleBank;

/**
 * Created by KW on 11/19/2017.
 */
public class Account {
    private double balance=0.0;

    public Account(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
