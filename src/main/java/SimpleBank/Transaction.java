package SimpleBank;

/**
 * Created by KW on 11/19/2017.
 */
public class Transaction {


    public void deposit(Account account, double amount) {
        synchronized (this) {
            account.setBalance(account.getBalance() - amount);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

public void withdraw(Account account, double amount) {
    synchronized (this) {
        account.setBalance(account.getBalance() - amount);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

    public double balance (Account account){
    return account.getBalance();
    }
}
