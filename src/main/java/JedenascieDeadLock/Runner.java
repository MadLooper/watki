package JedenascieDeadLock;

import java.util.Random;
import java.util.concurrent.locks.*;



public class Runner {
private Account acc1=new Account();
private Account acc2 = new Account();

private Lock lock1 = new ReentrantLock();
private Lock lock2=new ReentrantLock();

private void acquireLocks (Lock firstLock, Lock secondLock){
while (true){
    //acquire locks
    boolean gotFirstLock=false;
    boolean gotSecLock = false;
    try {
        gotFirstLock=firstLock.tryLock();
        gotSecLock= secondLock.tryLock();
    } finally {
        if (gotFirstLock && gotSecLock){
            return;
        }
        if (gotFirstLock ){
            firstLock.unlock();
        }
        if (gotSecLock ){
            secondLock.unlock();
        }
    }

    //locks not acquired
    try {
        Thread.sleep(1);
    } catch (InterruptedException e) {

    }
}
}
    public void firstThread() throws InterruptedException{
        Random random = new Random();
for (int i =0;i<10000;i++){
    acquireLocks(lock1,lock2);

    try {
    Account.transfer(acc1,acc2,random.nextInt(100));
}finally {
               lock1.unlock();
        lock2.unlock();
    }
    }
    }
    public void secThread() throws InterruptedException {
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            acquireLocks(lock1,lock2);

            try {
                Account.transfer(acc2, acc1, random.nextInt(100));
            } finally {
                lock1.unlock();
                lock2.unlock();
            }
        }
    }
         public void finished(){
        System.out.println("Account 1 balance: "+acc1.getBalance());
        System.out.println("Account 2 balance: "+acc2.getBalance());
        System.out.println("Total balance: "+ (acc1.getBalance()+acc2.getBalance()));

    }
}
