package AdvanceTrainStations;

import java.util.Scanner;

/**
 * Created by KW on 11/20/2017.
 */
public class Main {
    public static void main(String[] args) {
        Headquater headquater= new Headquater();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()){
            String line = scanner.nextLine();
            if (line.startsWith("start gdansk")){
                System.out.println("Startuje pociag z Gdanska");
                headquater.dispatchTrainFromGdansk();
            }else if (line.startsWith("start gdynia")){
                System.out.println("Startuje pociag z Gdyni");
                headquater.dispatchFromGdynia();
            }else if (line.startsWith("print")){
String stationId = line.replace("print ","")       ;
            int station_Id= Integer.parseInt(stationId);
            headquater.printTrainInfoOf(station_Id);
            }
        }
    }
}
