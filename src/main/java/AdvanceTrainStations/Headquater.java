package AdvanceTrainStations;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by KW on 11/19/2017.
 */
public class Headquater implements Observer {
    public final static Boolean DIRECTION_GDYNIA=false;//gdynia - stacja stations.length-1
    public final static Boolean DIRECTION_GDANSK=true;//gdansk - stacja 0

    private ExecutorService pociagi = Executors.newFixedThreadPool(10);

    private TrainSchedule scheduleFromGdansk;
    private TrainSchedule getScheduleFromGdynia;

    private List<Station>stationList;


    public Headquater() {
        this.scheduleFromGdansk = new TrainSchedule(new LinkedList<TrainScheduleRecord>(Arrays.asList(
                new TrainScheduleRecord(2000,false),
                new TrainScheduleRecord(5000,false),
                new TrainScheduleRecord(200,false),
                new TrainScheduleRecord(1000,false)
        )));

        this.getScheduleFromGdynia = new TrainSchedule(new LinkedList<TrainScheduleRecord>(Arrays.asList(
                new TrainScheduleRecord(3000,false),
                new TrainScheduleRecord(2000, false),
                new TrainScheduleRecord(5000, false),
                new TrainScheduleRecord(2000, false)
        )));
        this.stationList = new LinkedList<Station>(
                Arrays.asList(
                        new Station(0, scheduleFromGdansk, getScheduleFromGdynia),
                        new Station(1, scheduleFromGdansk, getScheduleFromGdynia),
                        new Station(2, scheduleFromGdansk, getScheduleFromGdynia),
                        new Station(3, scheduleFromGdansk, getScheduleFromGdynia),
                        new Station(4, scheduleFromGdansk, getScheduleFromGdynia),
                        new Station(5, scheduleFromGdansk, getScheduleFromGdynia),
                        new Station(6, scheduleFromGdansk, getScheduleFromGdynia),
                new Station(7, scheduleFromGdansk, getScheduleFromGdynia),
                new Station(8, scheduleFromGdansk, getScheduleFromGdynia),
                new Station(9, scheduleFromGdansk, getScheduleFromGdynia),
                new Station(10, scheduleFromGdansk, getScheduleFromGdynia),
                new Station(11, scheduleFromGdansk, getScheduleFromGdynia)

                ));
    }
public void dispatchTrainFromGdansk(){
        Train t= new Train(0,DIRECTION_GDANSK,scheduleFromGdansk);
        t.addObserver(this);
        pociagi.submit(t);
}

public void dispatchFromGdynia (){
    Train t = new Train(stationList.size() - 1,DIRECTION_GDYNIA,getScheduleFromGdynia);
    t.addObserver(this);
pociagi.submit(t);
}

    public void update(Observable o, Object arg) {
    if (o instanceof  Train){
        Train t = (Train) o;
        if (t.isDirection()== DIRECTION_GDYNIA){
            for (int i = t.getStationId();i<stationList.size();i++){
                stationList.get(i).update(t,arg);
            }

        }else {
            for (int i = t.getStationId();i<=0;i--){
                stationList.get(i).update(t,arg);
            }
        }
                    }

    }
    public void printTrainInfoOf(int id) {
        stationList.get(id).print();
    }
}
