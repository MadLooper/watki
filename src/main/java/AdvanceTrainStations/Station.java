package AdvanceTrainStations;
import java.util.*;
import java.util.Comparator;
import java.util.Observable;
import java.util.Observer;
import java.util.PriorityQueue;

/**
 * Created by KW on 11/19/2017.
 */
public class Station implements Observer {
    private  int id;
    private TrainSchedule fromGdansk;
    private TrainSchedule fromGdynia;

    private PriorityQueue listOfTrainsFromGdansk;
    private PriorityQueue<TrainInfo> listOfTrainsFromGdynia;

    public Station(int id, TrainSchedule fromGdansk, TrainSchedule fromGdynia) {
        this.id = id;
        this.fromGdansk = fromGdansk;
        this.fromGdynia = fromGdynia;
        this.listOfTrainsFromGdansk = new PriorityQueue(new TrainInfoComparator());
        this.listOfTrainsFromGdynia = new PriorityQueue(new TrainInfoComparator());
    }

    public void update(Observable o, Object arg) {

        if (o instanceof Train){
            Train t = (Train) o;
            System.out.println("Stacja o id: "+ id +" zostaje poinformowana o pociagu: "+t.getStationId());

            int obecnaStacja= t.getStationId();
            int naszaStacja =id;
            int idPociagu = t.getTrainId();

            if (obecnaStacja==naszaStacja) {
//                if (t.isDirection()) {
//                    listOfTrainsFromGdansk.remove(t);
//                } else {
//                    listOfTrainsFromGdynia.remove(t);
//                    //pociag dotarl do naszej stacji i usuwamy ho z kolejki priorytetowej
//                }
            }else {
                if (t.isDirection()==Headquater.DIRECTION_GDYNIA){
                    // jeśli jedzie w kierunku Gdynia to na stacji zero trzeba go dodać do kolejki priorytetowej
                    if (obecnaStacja == 0) {
                        // liczenie czasów dotarcia
                        long timeOfArrival = countTimeOfArrival(obecnaStacja, naszaStacja, fromGdansk);
                        listOfTrainsFromGdansk.add(new TrainInfo(timeOfArrival, idPociagu));
                    } else {
                        // trzeba go edytować
                    }
                } else {
                    // jeśli jedzie w kierunku gdańska to na stacji n-1 trzeba go dodać do kolejki priorytetowej
                    if (
                            obecnaStacja== fromGdynia.getSchedule().size()) {
                        // liczenie czasow dotarcia
                        long timeOfArrival = countTimeOfArrival(naszaStacja, obecnaStacja, fromGdynia);
                        listOfTrainsFromGdynia.add(new TrainInfo(timeOfArrival, idPociagu));
                    } else {
                        // trzeba go edytować
                    }
                    //
                }
            }
        }
        // lista wyświetla pociągi zgodnie z tylko tym, co ruszyło już ze stacji.
        // tutaj musi pojawić się "skomplikowana" logika oceny czasu dojazdu pociągu.
        // UWAGA! stacja nie wie o tym że pociąg nie zatrzymuje się na stacjach (innych niż ona sama)
        // UWAGA! (nie ma tego brać pod uwagę przy obliczaniu czasu dojazdu) więc pociagi beda zmienialy czasy dojazdow
        // Spróbuj użyć komparatora (liste listOfTrainsFromGdansk oraz listOfTrainsFromWladyslawowo zamien na priorityqueue)
    }


    public long countTimeOfArrival(int stacja_pociagu, int stacja_nasza, TrainSchedule rozklad) {
        long wynik = 0;
        for (int i = stacja_pociagu; i < stacja_nasza; i++) {
            wynik += rozklad.getSchedule().get(i).getTime();
        }
        return wynik;
    }

    public void print() {
        Iterator<TrainInfo> infos =listOfTrainsFromGdansk.iterator();
        while(infos.hasNext()){
            System.out.println(infos.next());
        }
    }

    public class TrainInfoComparator implements Comparator<TrainInfo> {
        public int compare(TrainInfo o1, TrainInfo o2) {
            return o1.getEstimatedTimeOfArrival() < o2.getEstimatedTimeOfArrival() ? -1 : 1;
        }
    }

}
