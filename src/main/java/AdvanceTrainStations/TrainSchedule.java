package AdvanceTrainStations;

import java.util.List;

/**
 * Created by KW on 11/19/2017.
 */
public class TrainSchedule {

    private List<TrainScheduleRecord> schedule;

    public TrainSchedule(List<TrainScheduleRecord> schedule) {
        this.schedule = schedule;
    }

    public List<TrainScheduleRecord> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<TrainScheduleRecord> schedule) {
        this.schedule = schedule;
    }
}
