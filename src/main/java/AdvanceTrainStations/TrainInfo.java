package AdvanceTrainStations;

/**
 * Created by KW on 11/19/2017.
 */
public class TrainInfo {

    private long estimatedTimeOfArrival;
    private int trainId;

    public TrainInfo(long estimatedTimeOfArrival, int trainId) {
        this.estimatedTimeOfArrival = estimatedTimeOfArrival;
        this.trainId = trainId;
    }

    public long getEstimatedTimeOfArrival() {
        return estimatedTimeOfArrival;
    }

    public void setEstimatedTimeOfArrival(long estimatedTimeOfArrival) {
        this.estimatedTimeOfArrival = estimatedTimeOfArrival;
    }

    public int getTrainId() {
        return trainId;
    }

    public void setTrainId(int trainId) {
        this.trainId = trainId;
    }

    @Override
    public String toString() {
        return "TrainInfo{" + "estimatedTimeOfArrival=" + estimatedTimeOfArrival + ", trainId=" + trainId + '}';
    }
}






