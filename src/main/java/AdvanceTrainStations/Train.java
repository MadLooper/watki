package AdvanceTrainStations;

import java.util.Observable;

/**
 * Created by KW on 11/19/2017.
 */
public class Train extends Observable implements Runnable {
    private static int trainCounter = 0;
    private int trainId = trainCounter++;

    private boolean isCancelled = false;
    private int stationId;
    private boolean direction; //if true - increment; false - decrement
    private TrainSchedule schedule;

    public Train(int stationId, boolean direction, TrainSchedule schedule) {
        this.isCancelled = isCancelled;
        this.stationId = stationId;
        this.direction = direction;
        this.schedule = schedule;
    }

    public static int getTrainCounter() {
        return trainCounter;
    }

    public static void setTrainCounter(int trainCounter) {
        Train.trainCounter = trainCounter;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public boolean isDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public TrainSchedule getSchedule() {
        return schedule;
    }

    public void setSchedule(TrainSchedule schedule) {
        this.schedule = schedule;
    }

    public int getTrainId() {
        return trainId;
    }

    public void setTrainId(int trainId) {
        this.trainId = trainId;
    }

    public void run() {
        System.out.println("Pociag zaczyna bieg");
        int scheduule_counter = 0;

        //informuje o stacji 0 - ruszamy
        setChanged();
        notifyObservers();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Pociag rusza ze stacji 0.");
        while (scheduule_counter < schedule.getSchedule().size()) {
            TrainScheduleRecord record = schedule.getSchedule().get(scheduule_counter);
            System.out.println("Pociag rusza ze stacji " + stationId);
            try {
                Thread.sleep(record.getTime());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (direction) {
                stationId--;
            } else {
                stationId++;
            }
            System.out.println("Pociag dotarl do stacji: " + stationId);
            setChanged();
            notifyObservers("Pociag wyruszyl dalej");

            if (!record.isSkip()) {
                System.out.println("Pociag zatrzymuje sie na stacji" + stationId);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Pociag pominal stacje" + stationId);
            }
            if (isCancelled) {
                System.out.println("Pociag zostal anulowany. ");
                break;
            }
            scheduule_counter++;
        }
            System.out.println("Pociag zakonczyl bieg. ");
            // każdy pociąg porusza się po linii
            // każdy pociąg jeśli zatrzymuje sie na stacji, to czeka na niej dokładnie 1 sek.
            // każdy pociąg może być anulowany
            // każdy pociąg informuje headquarters o tym że dojechał na stację, nawet jeśli nie zatrzymuje się na niej (nie czeka 1s.)
        }
        }


