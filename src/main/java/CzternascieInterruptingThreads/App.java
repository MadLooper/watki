package CzternascieInterruptingThreads;

import java.util.Random;

/**
 * Created by KW on 11/19/2017.
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Starting...");
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                Random random = new Random();
                for (int i=0;i<160;i++){

//
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        System.out.println("Interrupted: ");
                        break;
                    }
                    Math.sin(random.nextDouble());
                }
            }
        });
        t1.start();
        Thread.sleep(200);
        t1.interrupt();
        t1.join();
        System.out.println("finished");
    }
}
